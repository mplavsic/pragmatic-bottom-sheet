A package that turns working with modal bottom sheets into a breeze. This package is particularly useful when the requiring modal bottom sheets having a dynamic size. This is in contrast to the build-in function `showModalBottomSheet`, which — by default — takes half of the screen size.

## Features

* Pragmatic handle support with out-of-the-box round edge ends. Custom border radius and sharp edges are also supported. The handle is not enabled by default.
* Out-of-the-box round corners at the top of the bottom sheet (before the dimmed area). Custom border radius and sharp corners are also supported.
* Support for typical `showModalBottomSheet` arguments, such as `barrierColor` or `isDismissible`.

## Getting started

You need add `pragmatic_platform_utils` to your dependencies.

```yaml
dependencies:
  pragmatic_bottom_sheet:
    git:
      url: https://gitlab.com/mplavsic/pragmatic-bottom-sheet.git
      ref: main
```

## Usage

Once you import `package:pragmatic_bottom_sheet/pragmatic_bottom_sheet.dart`, you can use the top-level function `showPragmaticBottomSheet` and, if needed, the class `Handle`.

### Example with Wrap

```dart
showPragmaticBottomSheet(
    context: context,
    childWidget: Wrap(
        children: const [
            ListTile(title: Text('Title 1')),
            ListTile(title: Text('Title 2')),
            ListTile(title: Text('Title 3')),
        ],
        runSpacing: 20,
    ),
    handle: const Handle(),
),
```

N.B.: prefer using `Wrap` over `Column`, this way the bottom sheet will
not include a lot of white space in case all the children's
combined size is not enough to cover the whole space in the bottom sheet.

### Example with ListView

```dart
showPragmaticBottomSheet(
    context: context,
    childWidget: ListView(
    shrinkWrap: true,
        children: const [
            ListTile(title: Text('Title 1')),
            ListTile(title: Text('Title 2')),
            ListTile(title: Text('Title 3')),
        ],
    ),
    handle: const Handle(),
),
``` 

N.B.: for the same reason as in the example above, prefer using
`ListView` in combination with `shrinkWrap: true`.