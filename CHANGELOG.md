## 1.0.0

* This version comes already with everything needed for a pragmatic bottom sheet:
    * Pragmatic, resizable handle support with out-of-the-box round edge ends. Custom border radius and sharp edges are also supported. The handle is not enabled by default.
    * Out-of-the-box round corners at the top of the bottom sheet (before the dimmed area). Custom border radius and sharp corners are also supported.
    * Support for typical `showModalBottomSheet` arguments, such as `barrierColor` or `isDismissible`.