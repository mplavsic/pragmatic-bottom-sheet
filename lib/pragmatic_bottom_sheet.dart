library pragmatic_bottom_sheet;

import 'package:flutter/material.dart';

/// This function instantiates a modal bottom sheet that does not automatically
/// take half of the screen size (which is what happens by default when calling
/// [showModalBottomSheet]), instead — by default — this bottom sheet's height
/// will be just as much as the passed [childWidget] requires. Therefore,
/// all the developer needs to do is to pass a [childWidget] as a parameter and
/// specify if the bottom sheet also requires a handle or not.
///
/// Internally, this widget uses [SafeArea] to add space at the bottom to
/// support the gesture navigation handle on iOS devices.
/// This space is necessary otherwise the iOS system gesture navigation
/// handle would overlap with the bottom-most part of the [childWidget].
///
/// [shape] has precedence over [upperCornersCircularBorderRadius],
/// therefore either use [shape] or [upperCornersCircularBorderRadius].
///
/// [bottomSheetMaxHeightInScreenPercentage] ought to have a higher value than
/// [bottomSheetMaxContentHeightInScreenPercentage]'s one.
///
/// [distanceBetweenTitleAndChildWidget] is considered only if [title] is not
/// null.
void showPragmaticBottomSheet({
  // widget
  final Handle? handle,
  final Widget? title,
  final double distanceBetweenTitleAndChildWidget = 8.0,
  final double? spaceBeforeChildWidget,
  required final Widget childWidget,
  final double? spaceAfterChildWidget,
  // borders and proportion constrains
  final double? upperCornersCircularBorderRadius = 14.0,
  final double bottomSheetMaxWidthInPixels = 1200,
  final double bottomSheetMaxHeightInScreenPercentage = 0.78,
  final double bottomSheetMaxContentHeightInScreenPercentage = 0.58,
  // typical modal bottom sheet methods
  required final BuildContext context,
  final Color? backgroundColor, // = Colors.transparent,
  final double? elevation,
  final ShapeBorder? shape,
  final Clip? clipBehavior,
  final BoxConstraints? constraints,
  final Color? barrierColor,
  final bool useRootNavigator = false,
  final bool isDismissible = true,
  final bool enableDrag = true,
  final RouteSettings? routeSettings,
  final AnimationController? transitionAnimationController,
}) {
  showModalBottomSheet<void>(
    context: context,
    backgroundColor: backgroundColor,
    elevation: elevation,
    clipBehavior: clipBehavior,
    constraints: BoxConstraints(
      maxHeight: bottomSheetMaxHeightInScreenPercentage *
          MediaQuery.of(context).size.height,
      maxWidth: 1200,
    ),
    barrierColor: barrierColor,
    isScrollControlled: true,
    useRootNavigator: useRootNavigator,
    isDismissible: isDismissible,
    enableDrag: enableDrag,
    routeSettings: routeSettings,
    transitionAnimationController: transitionAnimationController,
    shape: shape ??
        ((upperCornersCircularBorderRadius == null)
            ? null
            : RoundedRectangleBorder(
                borderRadius: BorderRadius.vertical(
                  top: Radius.circular(upperCornersCircularBorderRadius),
                ),
              )),
    builder: (final BuildContext context) {
      return SafeArea(
        left: false,
        right: false,
        top: false,
        bottom: true,
        child: Wrap(
          runAlignment: WrapAlignment.start,
          crossAxisAlignment: WrapCrossAlignment.start,
          alignment: WrapAlignment.center,
          children: [
            Column(
              children: [
                if (handle != null) handle else const SizedBox(height: 12),
                if (title != null)
                  Column(
                    children: [
                      title,
                      SizedBox(height: distanceBetweenTitleAndChildWidget)
                    ],
                  ),
                Center(
                  child: LayoutBuilder(
                    builder:
                        (BuildContext context, BoxConstraints constraints) {
                      return Container(
                        constraints: BoxConstraints(
                          maxHeight:
                              bottomSheetMaxContentHeightInScreenPercentage *
                                  MediaQuery.of(context).size.height,
                        ),
                        child: childWidget,
                      );
                    },
                  ),
                ),
                const SizedBox(height: 2),
              ],
            ),
          ],
        ),
      );
    },
  );
}

/// The handle that is on top that helps dismissing the modal bottom sheet.
///
/// [widthSpacePercentage] is the percentage of the MediaQuery width needed by
/// the handle.
///
///
/// [minWidth] and [maxWidth] are constraints to prevent tinily or enormously
/// wide handles.
/// To ignore [minWidth], set it to 0.
/// To ignore [maxWidth], set it to [double.infinity].
class Handle extends StatelessWidget {
  final Color color;
  final double height;

  /// Percentage that represents the amount of available horizontal space the
  /// handle should take.
  final double widthSpacePercentage;

  /// To ignore this constraint, set it to 0.
  final double minWidth;

  /// To ignore this constraint, set it to [double.infinity].
  final double maxWidth;

  /// Specifies how curvy the handle should be. Set to null if you don't want
  /// the handle to have a circular border radius.
  final double? circularBorderRadius;

  final double distanceBetweenDimmedAreaAndHandle;

  /// The child in the modal bottom sheet.
  final double distanceBetweenHandleAndChildWidget;

  const Handle({
    Key? key,
    this.color = const Color(0xffd9d9d9),
    this.height = 5.0,
    this.widthSpacePercentage = 0.15,
    this.minWidth = 40,
    this.maxWidth = 140,
    this.circularBorderRadius = 10.0,
    this.distanceBetweenDimmedAreaAndHandle = 10.0,
    this.distanceBetweenHandleAndChildWidget = 14.0,
  }) : super(key: key);

  @override
  Widget build(final BuildContext context) {
    return LayoutBuilder(
      builder: (final BuildContext context, final BoxConstraints constraints) {
        return Column(
          children: [
            SizedBox(height: distanceBetweenDimmedAreaAndHandle),
            Container(
              constraints: BoxConstraints(
                minWidth: minWidth,
                maxWidth: maxWidth,
              ),
              // the width is responsive
              width: MediaQuery.of(context).size.width * widthSpacePercentage,
              height: height,
              decoration: BoxDecoration(
                borderRadius: circularBorderRadius == null
                    ? null
                    : BorderRadius.circular(circularBorderRadius!),
                color: color,
              ),
            ),
            SizedBox(height: distanceBetweenHandleAndChildWidget),
          ],
        );
      },
    );
  }
}

/// Dismisses the current bottom sheet and waits [ms] milliseconds. The default
/// value of [ms] is 400 ms.
///
/// A call to this function should be followed to a call to
/// [showPragmaticBottomSheet].
///
/// N.B.: This function can be called from any screen, however the intended use
/// is from within a bottom sheet.
Future<void> popBottomSheetAndWait({
  required final BuildContext context,
  final int ms = 400, 
}) async {
  Navigator.pop(context);
  await Future.delayed(Duration(milliseconds: ms));
}
